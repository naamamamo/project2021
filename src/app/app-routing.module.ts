import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignUpComponent } from './signup/signup.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { HotelsComponent } from './hotels/hotels.component';
import { BookingFormComponent } from './booking-form/booking-form.component';
import { PredictionTableComponent } from './prediction-table/prediction-table.component';
import { CancelTableComponent } from './cancel-table/cancel-table.component';
import { CancelFormComponent } from './cancel-form/cancel-form.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TodoComponent } from './todo/todo.component';
import { ParkingComponent } from './parking/parking.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'home', component: WelcomeComponent },
  { path: 'myorders', component: HotelsComponent },
  { path: 'form', component: BookingFormComponent },
  { path: 'table', component: PredictionTableComponent },
  { path: 'cancelForm/:hotelId/', component: CancelFormComponent },
  { path: 'cancelTable', component: CancelTableComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'todo', component: TodoComponent },
  { path: 'parkingForm/:hotelId', component: ParkingComponent }



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
