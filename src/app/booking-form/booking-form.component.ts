import { CurrencyService } from './../currency.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { HotelService } from '../hotel.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';


interface Hotel {
  value: string;
  viewValue: string;
}

interface Meal {
  value: string;
  viewValue: string;
}

interface Country {
  value: string;
  viewValue: string;
}

interface Deposit {
  value: string;
  viewValue: string;
}

interface Payment {
  value: string;
  viewValue: number;
}

interface Year {
  value: string;
  viewValue: number;
}

interface Month {
  value: string;
  viewValue: number;
}

@Component({
  selector: 'app-booking-form',
  templateUrl: './booking-form.component.html',
  styleUrls: ['./booking-form.component.css']
})


export class BookingFormComponent implements OnInit {
  isLinear=true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  range = new FormGroup({
  start: new FormControl(),
  end: new FormControl()
  });


  userId:string; 
  start;
  end;
  between;
  price;
  rate;
  localPrice;
  symbol;
  coin;
  now = new Date();
  today = moment(this.now).format('YYYY-MM-DD');
  start1;
  end1;
  hotelId;

  leadtime;

  countries: Country[] = [
    {value: 'Austria', viewValue: 'Austria'},
    {value: 'Belgium', viewValue: 'Belgium'},
    {value: 'Brazil', viewValue: 'Brazil'},
    {value: 'Switzerland', viewValue: 'Switzerland'},
    {value: 'China', viewValue: 'China'},
    {value: 'Germany', viewValue: 'Germany'},
    {value: 'Spain', viewValue: 'Spain'},
    {value: 'France', viewValue: 'France'},
    {value: 'United Kingdom', viewValue: 'United Kingdom'},
    {value: 'Irland', viewValue: 'Irland'},
    {value: 'Italy', viewValue: 'Italy'},
    {value: 'Netherlands', viewValue: 'Netherlands'},
    {value: 'Portugal', viewValue: 'Portugal'},
    {value: 'Sweden', viewValue: 'Sweden'},
    {value: 'United State', viewValue: 'United State'},
    {value: 'Israel', viewValue: 'Israel'},
  ];

  hotels: Hotel[] = [
    {value: 'Resort Hotel', viewValue: 'Resort Hotel'},
    {value: 'City Hotel', viewValue: 'City Hotel'},
  ]; 

  meals: Meal[] = [
    {value: 'SC', viewValue: 'Self Catering'},
    {value: 'HB', viewValue: 'half board'},
    {value: 'FB', viewValue: 'full board'},
    {value: 'BB', viewValue: 'bed and breakfast'},
  ];

  deposits: Deposit[] = [
    {value: 'No Deposit', viewValue: 'No Deposit'},
    {value: 'Refundable', viewValue: 'Refundable'},
    {value: 'Non Refund', viewValue: 'Non Refund'},
    ]; 

  payments: Payment[] = [
    {value: 'one', viewValue: 1},
    {value: 'two', viewValue: 2},
    {value: 'three', viewValue: 3},
    {value: 'four', viewValue: 4},
    {value: 'five', viewValue: 5},
    {value: 'six', viewValue: 6},
    {value: 'seven', viewValue: 7},
    {value: 'eight', viewValue: 8},
    {value: 'nine', viewValue: 9},
    {value: 'ten', viewValue: 10}
  ]; 

  years: Year[] = [
    {value: 'twenty-one', viewValue: 2021},
    {value: 'twenty-two', viewValue: 2022},
    {value: 'twenty-three', viewValue: 2023},
    {value: 'twenty-four', viewValue: 2024},
    {value: 'twenty-five', viewValue: 2025},
    {value: 'twenty-six', viewValue: 2026},
    {value: 'twenty-seven', viewValue: 2027},
    {value: 'twenty-eight', viewValue: 2028},
    {value: 'twenty-nine', viewValue: 2029},
    {value: 'thirty', viewValue: 2030}
  ]; 

  months: Month[] = [
    {value: 'January', viewValue: 0o1},
    {value: 'February', viewValue: 0o2},
    {value: 'March', viewValue: 0o3},
    {value: 'April', viewValue: 0o4},
    {value: 'May', viewValue: 0o5},
    {value: 'June', viewValue: 6},
    {value: 'July', viewValue: 7},
    {value: 'August', viewValue: 8},
    {value: 'September', viewValue: 9},
    {value: 'October', viewValue: 10},
    {value: 'November', viewValue: 11},
    {value: 'Decemver', viewValue: 12}

  ];


  navigateToMyOrders() {
    this.router.navigate(['/myorders']);
    this.dialog.open(DialogComponent);
  }

  constructor(private formBuilder:FormBuilder ,private hotelService:HotelService, private authService:AuthService, private currencyService:CurrencyService,private router:Router, public dialog: MatDialog) {}
  onSubmit() {
    this.hotelService.addHotles(this.userId,
                                this.firstFormGroup.get('firstname').value,
                                this.firstFormGroup.get('lastname').value,
                                this.start,
                                this.end, 
                                this.firstFormGroup.get('hotel').value,
                                this.firstFormGroup.get('room').value || 1,
                                this.firstFormGroup.get('adults').value,
                                this.firstFormGroup.get('children').value || 0,
                                this.firstFormGroup.get('babies').value || 0,
                                this.firstFormGroup.get('meal').value,
                                this.firstFormGroup.get('country').value,
                                this.firstFormGroup.get('deposit').value,
                                this.today,
                                this.price); 
  }

  
// allUsers(hotelId, firstname:string,lastname:string,price:number,hotel:string){
//   this.hotelId = hotelId;
//   this.hotelService.addCancel(hotelId,
//                             this.firstFormGroup.get('firstname').value,
//     );
//   alert(hotelId);

// }

  Calc(){
    this.start = moment(this.firstFormGroup.get('start').value).format('YYYY-MM-DD');
    this.end = moment(this.firstFormGroup.get('end').value).format('YYYY-MM-DD');
    this.start1 = new Date(this.start);
    this.end1 = new Date(this.end);
    this.between = Math.round(Math.abs(this.end1 - this.start1) / (24 * 3600 * 1000));
    if(this.firstFormGroup.get('hotel').value == 'Resort Hotel'){
      this.price = 49 * this.between * (this.firstFormGroup.get('adults').value + (this.firstFormGroup.get('children').value/2))
    }
    else{
      this.price = 27 * this.between * (this.firstFormGroup.get('adults').value + (this.firstFormGroup.get('children').value/2))
    }
    this.currencyService.getRate(this.firstFormGroup.get('country').value).then(
      res =>{
        this.rate = res;
        this.localPrice = this.rate * this.price;
      }
    );
    this.symbol = this.currencyService.countries.find(x=>x.name == this.firstFormGroup.get('country').value).symbol;
    this.coin = this.currencyService.countries.find(x=>x.name == this.firstFormGroup.get('country').value).coin;
  }
  
  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);});

    this.firstFormGroup = this.formBuilder.group({
      firstname: [null, Validators.required],
      lastname: [null, Validators.required],
      start: [null, Validators.required],
      end: [null, Validators.required],
      hotel: [null, Validators.required],
      room: [''],
      adults: [null, Validators.required],
      children: [''],
      babies: [''],
      meal: [null, Validators.required],
      country: [null, Validators.required],
      deposit: [null, Validators.required],


    });

 
    this.secondFormGroup = this.formBuilder.group({
      payments: ['', Validators.required],
      saved: [''],
      id: ['', Validators.required],
      credit: ['', Validators.required],
      cvv:  ['', Validators.required],
      year:  ['', Validators.required],
      month:  ['', Validators.required],
    });

    this.thirdFormGroup = this.formBuilder.group({
      id: ['', Validators.required],
    });
    
}  
    
  }


function round(arg0: number, arg1: number): any {
  throw new Error('Function not implemented.');
}

function ToMyOrders(){
  this.router.navigate(['/myorders'])
}



