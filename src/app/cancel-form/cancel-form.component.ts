import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { HotelService } from '../hotel.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DialogElementsExampleDialogComponent } from '../dialog-elements-example-dialog/dialog-elements-example-dialog.component';

@Component({
  selector: 'app-cancel-form',
  templateUrl: './cancel-form.component.html',
  styleUrls: ['./cancel-form.component.css']
})


export class CancelFormComponent implements OnInit {
  [x: string]: any;

  hotelId:string;
  userId;
  id:string;
  selectedReason:string;
  hotels$;
  reasons1=[];
  animal: string;
  name: string;
  reasons: string[] = ['Found a different accommodation option',
                       'Unable to travel due to restrictions related to coronavirus (COVID-19)',
                       'Change in the number or needs of travelers',
                       'Change of dates or destinaton',
                       'Made bookings for the same dates - canceled the ones I dont need',
                       'Personal reasons/Trip called off',
                       'other'];

                       
                       

  constructor(public hotelService:HotelService, public authService:AuthService,
              private route:ActivatedRoute,private router:Router, public dialog: MatDialog) { }
              
  openDialog() {
    this.dialog.open(DialogElementsExampleDialogComponent);
              }
          

  Reason(hoteld:string,reason:string){
    this.hotelService.updateReason(this.userId,hoteld,reason);
  }


  navigateToMyOrders() {
    this.router.navigate(['/myorders']);
  }

  ngOnInit(): void {
    this.hotelId = this.route.snapshot.params.hotelId;
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.useremail = user.email;
        console.log(this.userId); 
        this.hotels$ = this.hotelService.getHotles(this.userId);
        
        this.hotels$.subscribe(
          docs =>{
            this.hotels = [];
            for(let document of docs){
              const hotel:any = document.payload.doc.data();
              hotel.id = document.payload.doc.id; 
              this.hotels.push(hotel); 
            }
          }
        ) 
        }
      )
  }
}

