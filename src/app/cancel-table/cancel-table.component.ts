import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { HotelService } from '../hotel.service';
import { Customer } from '../interfaces/customer';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';


@Component({
  selector: 'app-cancel-table',
  templateUrl: './cancel-table.component.html',
  styleUrls: ['./cancel-table.component.css']
})
export class CancelTableComponent implements OnInit {
  displayedColumns: string[] = ['orderId','today','firstname','lastname','useremail','hotel','price','reason'];
  dataSource = new MatTableDataSource<Customer>()  


  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private hotelService:HotelService) { }

 
  ngOnInit(): void { 
    this.hotelService.getCancel().subscribe(
      res => this.dataSource.data = res,
      

      )}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
          }
        }
        
        


