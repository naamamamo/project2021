import { query } from '@angular/animations';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  private URL = "https://free.currconv.com/api/v7/convert?q=EUR_";
  private KEY = "974ea66a9f8259cb699b";
  items = [];

  countries = [
    {name:"Israel", coin: "ILS", symbol:"₪"},
    {name:"United State", coin: "USD", symbol:"$"},
    {name:"Austria", coin: "EUR", symbol:"€"},
    {name:"Belgium", coin: "EUR", symbol:"€"},
    {name:"Brazil", coin: "BRL", symbol:"R$"},
    {name:"Switzerland", coin: "CHF", symbol:"€"},
    {name:"China", coin: "CNY", symbol:"¥"},
    {name:"Germany", coin: "EUR", symbol:"€"},
    {name:"Spain", coin: "EUR", symbol:"€"},
    {name:"France", coin: "EUR", symbol:"€"},
    {name:"United Kingdom", coin: "GBP", symbol:"£"},
    {name:"Irland", coin: "EUR", symbol:"€"},
    {name:"Italy", coin: "EUR", symbol:"€"},
    {name:"Netherlands", coin: "EUR", symbol:"€"},
    {name:"Portugal", coin: "EUR", symbol:"€"},
    {name:"Sweden", coin: "EUR", symbol:"€"},
  ];

  constructor(private http:HttpClient) { }


  getRate(country:string){
    var currency = this.countries.find(x=>x.name == country).coin;
    return this.http.get(`${this.URL}${currency}&compact=ultra&apiKey=${this.KEY}`).toPromise().then(
      res => {
        console.log(res);
        for (let key in res)
          if (res.hasOwnProperty(key))
            this.items.push(res[key]);
       
       return this.items;     
      }
    )
  }

  






}
