import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip, SingleDataSet } from 'ng2-charts';
import { HotelService } from '../hotel.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  //KPI Vars
  orders:number;
  cancells:number;
  cancellationRate:number;

  //Pie Vars
  countResort = 0;
  countCity = 0;
  countElements = 0;

  //Bar Vars
  Austria = 0;
  Belgium = 0;
  Brazil = 0; 
  Switzerland = 0;
  China = 0;
  Germany = 0;
  Spain = 0;
  France = 0;
  UK = 0; 
  Irland = 0;
  Italy = 0;
  Netherlands = 0;
  Portugal = 0;
  Sweden = 0; 
  USA = 0;
  Israel = 0;

  // Pie Chart //
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = [['City Hotel'], ['Resort Hotel']];
  public pieChartData: SingleDataSet = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  public chartColors: Array<any> = [
    {
      backgroundColor: ['#46BFBD', '#FDB45C'],
      hoverBackgroundColor: ['#5AD3D1', '#FFC870'],
      borderWidth: 2,
    }
  ];


  // Bar Chart //
  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[] = ['Austria', 'Belgium', 'Brazil', 'Switzerland', 'China', 'Germany','Spain', 'France', 'UK', 'Irland', 'Italy', 'Netherlands', 'Portugal', 'Sweden', 'USA', 'Israel'];
  barChartType: ChartType = 'bar';
  barChartLegend = false;
  barChartPlugins = [];

  barChartData: ChartDataSets[] = [];




  constructor(private hotelService:HotelService) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
   }


  ngOnInit(): void {
    this.hotelService.getAllHotels().subscribe(
      res => {
      this.orders = res.length;
      }
    )
    this.hotelService.getCancel().subscribe(
      res => {
        this.cancells = res.length;
      }
    )
    this.hotelService.getAllHotels().subscribe(
      res => res.forEach(element =>{
        this.countElements ++
        if(element.hotel=='City Hotel'){
          this.countCity ++ 
        } else {
          this.countResort ++
        }

        if(element.country=='Austria'){
          this.Austria ++ 
        }
        else if(element.country=='Belgium'){
          this.Belgium ++ 
        }
        else if(element.country=='Brazil'){
          this.Brazil ++ 
        } 
        else if(element.country=='Switzerland'){
          this.Switzerland ++ 
        } 
        else if(element.country=='China'){
          this.China ++ 
        } 
        else if(element.country=='Germany'){
          this.Germany ++ 
        } 
        else if(element.country=='Spain'){
          this.Spain ++ 
        } 
        else if(element.country=='France'){
          this.France ++ 
        } 
        else if(element.country=='UK'){
          this.UK ++ 
        } 
        else if(element.country=='Irland'){
          this.Irland ++ 
        } 
        else if(element.country=='Italy'){
          this.Italy ++ 
        } 
        else if(element.country=='Netherlands'){
          this.Netherlands ++ 
        } 
        else if(element.country=='Portugal'){
          this.Portugal ++ 
        } 
        else if(element.country=='Sweden'){
          this.Sweden ++ 
        } 
        else if(element.country=='USA'){
          this.USA ++ 
        } 
        else{
          this.Israel ++ 
        } 
      }) 
      )
    }


  }





