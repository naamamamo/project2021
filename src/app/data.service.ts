import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Todo } from './interfaces/todo';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  todoApi: string = 'http://ec2-34-224-94-118.compute-1.amazonaws.com/todo';

  genericApi: string = 'http://localhost:3000/generics';

  get_todo() {
    return this.httpClient.get<Todo[]>(`${this.todoApi}`);
  }

// Dynamodb connection

fileUpload(file: File) {
  const formData: FormData = new FormData();
  formData.append("photo", file, file.name)

  return this.httpClient.post('http://ec2-34-224-94-118.compute-1.amazonaws.com/uploadphoto',formData) 
}

analyze(filename) {
  return this.httpClient.get('http://ec2-34-224-94-118.compute-1.amazonaws.com/analyze/chilki-upload-bucket/'+filename) 
}

SaveCusDetails(hotelId,license_plate:string):Observable<any> {
  let json ={
  "license_plate":license_plate,
  "hotelId":hotelId
  };
  
  console.log(json);
  
  const formData: FormData = new FormData();
  formData.append("data", JSON.stringify(json))
  
  // return this.httpClient.post('http://34.202.233.173/set_form', formData)
  return this.httpClient.post('http://ec2-34-224-94-118.compute-1.amazonaws.com/set_form', json)
}

updateCusDetails(hotelId:string, car_License_num:string):Observable<any> {
  let json ={"form_id":hotelId,
             "Car_License_num":car_License_num

  };
  return this.httpClient.post('http://ec2-34-224-94-118.compute-1.amazonaws.com/update_form', json)
}


sendSMS(car_License_num:string, phoneNum:string):Observable<any>{
  let json ={"phoneNum":phoneNum,
  "car_License_num":car_License_num
  };
  return this.httpClient.post('http://ec2-34-224-94-118.compute-1.amazonaws.com//sendsms', json);
}


  constructor(private httpClient: HttpClient) { }
}

