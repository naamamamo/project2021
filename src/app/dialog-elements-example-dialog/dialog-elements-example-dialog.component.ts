import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dialog-elements-example-dialog',
  templateUrl: './dialog-elements-example-dialog.component.html',
  styleUrls: ['./dialog-elements-example-dialog.component.css']
})
export class DialogElementsExampleDialogComponent implements OnInit {


  constructor(private router:Router) { }

  navigateToMyOrders() {
    this.router.navigate(['/myorders']);
  }

  ngOnInit(): void {
  }

}
