import { Customer } from './interfaces/customer';
import { formatCurrency } from '@angular/common';
import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  hotelCollection:AngularFirestoreCollection; 
  AllHotelsCollection:AngularFirestoreCollection = this.db.collection('allusers');
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  CancelCollection:AngularFirestoreCollection;
  

  public getHotles(userId){
    this.hotelCollection = this.db.collection(`users/${userId}/hotels`,  ref => ref.orderBy('start', 'asc')); 
    return this.hotelCollection.snapshotChanges();
    }  

  addHotles(userId:string,firstname:string,lastname:string,start:number,end:number, hotel:string, room:number, adults:number, children:number, babies:number, meal:string, country:string, deposit:string, today:any, price:number){
    // const hotels = {firstname:firstname, lastname:lastname,start:start, end:end, hotel:hotel, room:room, adults:adults, children:children, babies:babies, meal:meal, country:country, deposit:deposit, today:today, price:price};
    // this.userCollection.doc(userId).collection('hotels').add(hotels);
    var number = Math.random();
    var id = number.toString(36).substr(2,9);
    const ref = this.db.collection('users').doc(userId).collection('hotels').doc(id).set({
      firstname:firstname,
      lastname:lastname,
      start:start,
      end:end,
      hotel:hotel,
      room:room, 
      adults:adults, 
      children:children, 
      babies:babies, 
      meal:meal, 
      country:country, 
      deposit:deposit, 
      today:today, 
      price:price   
     });   
    this.db.collection('allusers').doc(id).set({
      firstname:firstname,
      lastname:lastname,
      start:start,
      end:end,
      hotel:hotel,
      room:room, 
      adults:adults, 
      children:children, 
      babies:babies, 
      meal:meal, 
      country:country, 
      deposit:deposit, 
      today:today, 
      price:price   
     })
     }

  addCancel(hotelId, firstname:string,lastname:string,price:number,today:any,useremail:string,hotel:string){
    this.db.collection('cancel').doc(hotelId).set({
      today:today,
      firstname:firstname,
      lastname:lastname,
      useremail:useremail,
      hotel:hotel,
      price:price,
    })
    
}

delete(hotelID){
  this.db.doc(`allusers/${hotelID}`).delete();
}

  updateReason(userId,id:string,reason:string){
    this.db.doc(`users/${userId}/hotels/${id}`).update(
      {
        reason:reason
      } )
      this.db.doc(`cancel/${id}`).update(
        {
          reason:reason
        } )

    }


      
  public getAllHotels():Observable<any[]>{
    this.AllHotelsCollection = this.db.collection(`allusers`);
    return this.AllHotelsCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return { ...data };
      }))
    ); 
  }
   
  public getCancel():Observable<any[]>{
    this.CancelCollection = this.db.collection('cancel');
    return this.CancelCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return { ...data };
      }))
    ); 
  }
   


  constructor(private db:AngularFirestore) { }
}
