import { Observable } from 'rxjs';
import { Customer } from './../interfaces/customer';
import { AuthService } from './../auth.service';
import { HotelService } from './../hotel.service';
import { DataService } from './../data.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';



@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {

  hotels$;
  hotelId:string;
  userId:string;
  hotels:any[];
  panelOpenState = false;
  now = new Date();
  today = moment(this.now).format('YYYY-MM-DD');
  useremail:string;


  car_License_num:string;
  firstname:string;
  lastname:string;
  editstate = [];
  constructor(private hotelService:HotelService,
              private DataService:DataService,
              public authService:AuthService,
              private router:Router) { }

  SaveHotelId(hotelId, firstname:string,lastname:string,price:number,hotel:string){
    this.hotelId = hotelId;
    this.hotelService.addCancel(hotelId, firstname,lastname,price,this.today,this.useremail,hotel);
    this.router.navigate(['/cancelForm',this.hotelId]);

  }
// Dynamodb
public go(hotelId, firstName:string,lastName:string){
  this.router.navigate(['/parkingForm',hotelId]);
}


  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.useremail = user.email;
        console.log(this.userId); 
        this.hotels$ = this.hotelService.getHotles(this.userId);
        
        this.hotels$.subscribe(
          docs =>{
            this.hotels = [];
            for(let document of docs){
              const hotel:any = document.payload.doc.data();
              hotel.id = document.payload.doc.id; 
              this.hotels.push(hotel); 
            }
          }
        ) 
        }
      )
  }
}

function firstname(firstname: any, arg1: any) {
  throw new Error('Function not implemented.');
}
