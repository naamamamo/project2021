export interface Customer {
    id:string,
    reason:string;
    adults:number;
    children: number,
    country: string,
    deposit:string,
    end:Date,
    firstname:string,
    hotel:string,
    lastname:string,
    meal:string,
    room:number,
    start:Date,
    timestamp?:Date,
    adr?:number,
    isCnacelled?:boolean,
    result?:string,
    saved?:boolean






}



