import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password;
  isError:boolean = false;
  errorMessage:string; 
  hide = true;
  userId;
  useremail;
  isAdmin = false;


  onSubmit(useremail:string){
    this.auth.login(this.email,this.password).then(res => {
      console.log(res);
      if(useremail=='admin@admin.com'){
        this.router.navigate(['/dashboard'])
      }
      else{
        this.router.navigate(['/home'])
      }
  }).catch(
      err => {
        console.log(err);
        this.isError = true; 
        this.errorMessage = err.message; 
      } 
    ) 
  }
  constructor(private auth:AuthService, private router:Router) { }

  ngOnInit(): void {
    this.auth.user.subscribe(
      user => {
      this.userId = user.uid;
      this.useremail = user.email;
      console.log(this.useremail);
      if(this.useremail=='admin@admin.com'){
        this.isAdmin = true;
      } 
    }
    )
    }

  }

  