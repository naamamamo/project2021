import { Component, EventEmitter, Output } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {
  userId:string;
  useremail:string;
  isAdmin = false;


  @Output() SideNavToggle = new EventEmitter();

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

    logout(){
      this.authService.logout();
      this.router.navigate(['/home'])
 
    }

  constructor(private breakpointObserver: BreakpointObserver,
              public authService:AuthService,
              private router:Router) {}

          
             
    ngOnInit() {
      this.authService.user.subscribe(
        user => {
        this.userId = user.uid;
        this.useremail = user.email;
        console.log(this.useremail);
        if(this.useremail=='admin@admin.com'){
          this.isAdmin = true;
        } 

      }
      )
      }        
      }
            
              
        
