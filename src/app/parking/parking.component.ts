import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { HotelService } from './../hotel.service';
import { DataService } from './../data.service';

@Component({
  selector: 'parking',
  templateUrl: './parking.component.html',
  styleUrls: ['./parking.component.css']
})
export class ParkingComponent implements OnInit {

  userId;
  id:string;
  hotels$;
  animal: string;
  name: string;
  hotelId;
  licenseNum:string;
  PhoneNum:string;

  constructor(public hotelService:HotelService, public authService:AuthService,
    private route:ActivatedRoute,private router:Router, public dialog: MatDialog,private DataService:DataService) { }

    
    navigateToMyOrders() {
      this.router.navigate(['/myorders']);
    } 

    public SaveDynamo(hotelId, licenseNum:string){
      console.log('inside my to dos');
      this.DataService.SaveCusDetails(hotelId,licenseNum).subscribe(res =>{
        console.log(res);
      });
      this.hotelId = hotelId;
    }

    toUpdate(hotelId:string){
      this.DataService.updateCusDetails(hotelId,this.licenseNum).subscribe(res =>{
        console.log(res);
      });
      this.router.navigate(['/myorders']);
    }

    onSubmit(){
      this.DataService.sendSMS(this.licenseNum,this.PhoneNum).subscribe(res =>{
        console.log(res);
      });
     
    }
    
  ngOnInit(): void {
    this.hotelId = this.route.snapshot.params.hotelId;
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.hotels$ = this.hotelService.getHotles(this.userId);

        }
      )
  }

}
