import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {
  private url = "https://v7ci469akc.execute-api.us-east-1.amazonaws.com/hotels-beta";

  //private result:object = {0:'Will arrive', 1:'Will cancel'}

  predictOrder(id,data){
    let json = {'data':data}
    let body = JSON.stringify(json);
    return this.HttpClient.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res);
        if(res[0] == 0){
          var prediction = 'Will arrive';
        }
        else if (res[0] == 1) {
          var prediction = 'Will cancel'
        }
        this.db.doc(`allusers/${id}`).update({
          prediction:prediction
        })
        console.log(res);
        return res;
      })
    )
  }

  predictOrders(listID,list){
    let json = {'data':list}
    let body = JSON.stringify(json);
    return this.HttpClient.post<any>(this.url, body).pipe(
      map(res =>{
        var i = 0;
        res.forEach(element =>{
          if(element == 0){
            var prediction = 'Will arrive';
          }
          else if(element == 1){
            var prediction = 'Will cancel';
          }
          this.db.doc(`allusers/${listID[i]}`).update({prediction:prediction})
          i++;
        })
      })
    )
  }

  constructor(private HttpClient:HttpClient,
              private db:AngularFirestore) { }
}
