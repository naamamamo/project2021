import { PredictService } from './../predict.service';
import { Customer } from './../interfaces/customer';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { HotelService } from '../hotel.service';
import { MatTableDataSource } from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';



@Component({
  selector: 'app-prediction-table',
  templateUrl: './prediction-table.component.html',
  styleUrls: ['./prediction-table.component.css']
})
export class PredictionTableComponent implements OnInit {
  displayedColumns: string[] = ['start','end','hotel','firstname','lastname','country','deposit','price','meal','adults','children','today','predict'];
  dataSource = new MatTableDataSource<Customer>()  
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  
  userId:string;
  hotels$
  customers:Customer [];
  customer:Customer;
  start;
  timestamp;
  between;
  s;
  t;
  prediction;
  

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  
  constructor(private hotelService:HotelService, public authService:AuthService, private predictService:PredictService) { }
    
  Calc(index){
    this.timestamp =(this.customers[index].timestamp);
    console.log(this.timestamp)
    this.start = new Date(this.customers[index].start);
    console.log(this.start)
    this.between = Math.round(Math.abs(this.start.getTime() - this.timestamp.nanoseconds)/(10000000*3600*24));
    console.log(this.between)
    return this.between;
  }



  hotels = [
    {name:"City Hotel", index: 0},
    {name:"Resort Hotel", index: 1},
  ];

  months = [
    {name:"01", index: 1},
    {name:"02", index: 2},
    {name:"03", index: 3},
    {name:"04", index: 4},
    {name:"05", index: 5},
    {name:"06", index: 6},
    {name:"07", index: 7},
    {name:"08", index: 8},
    {name:"09", index: 9},
    {name:"10", index: 10},
    {name:"11", index: 11},
    {name:"12", index: 12},
  ];

  meals = [
    {name:"FB", index: 0},
    {name:"BB", index: 1},
    {name:"SC", index: 2},
    {name:"HB", index: 3},
  ];

  countries = [
    {name: 'Austria', index: 14},
    {name: 'Belgium', index: 8},
    {name: 'Brazil', index: 9},
    {name: 'Switzerland', index: 12},
    {name: 'China', index: 13},
    {name: 'Germany', index: 5},
    {name: 'Spain', index: 4},
    {name: 'France', index: 3},
    {name: 'United Kingdom', index: 1},
    {name: 'Irland', index: 7},
    {name: 'Italy', index: 6},
    {name: 'Netherlands', index: 10},
    {name: 'Portugal', index: 0},
    {name: 'Sweden', index: 15},
    {name: 'United State', index: 11},
    {name: 'Israel', index: 2},
  ];

  deposits = [
    {name:"No Deposit", index: 0},
    {name:"Non Refund", index: 1},
    {name:"Refundable", index: 2},
  ];

  elementToJson(element){
    var hotel = this.hotels.find(x=>x.name == element.hotel).index;
    var s:any = new Date(element.start);
    var t:any = new Date(element.today);
    var leadtime = Math.round(Math.abs(s-t) / (24 * 3600 * 1000));
    var month = element.start.substring(5,7);
    var arrivalDateMonth = this.months.find(x=>x.name == month).index;
    var meal = this.meals.find(x=>x.name == element.meal).index;
    var country = this.countries.find(x=>x.name == element.country).index;
    var deposit = this.deposits.find(x=>x.name == element.deposit).index;

    return {
      "hotel": hotel, 
      "leadtime": leadtime, 
      "arrivalDateMonth": arrivalDateMonth, 
      "adults": element.adults, 
      "children": element.children, 
      "meal": meal, 
      "country": country, 
      "deposit": deposit, 
      "price": element.price
    }
  }


  sendData(element){
    var j = this.elementToJson(element)

    this.predictService.predictOrder(element.id,[j]).subscribe(
          res => {
          
          }
        )
    

  }

  sendAllData(){
    var list = []
    var listID = []
    this.dataSource.data.forEach(element => {
      var j = this.elementToJson(element)
      list.push(j)
      var id = element.id
      listID.push(id)
    });
    this.predictService.predictOrders(listID,list).subscribe(
      res => {
        console.log(res);
      }                       
  )
}


  ngOnInit(): void { 
    this.hotelService.getAllHotels().subscribe(
      res => this.dataSource.data = res
  
      )}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

      }
    }
    
    
    
  
    
   

        
    
    