import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignUpComponent implements OnInit {

  email:string;
  password:string;
  errorMessage:string;
  isError:boolean = false;
  hide = true;
 
  

  constructor(private authService:AuthService, private router:Router) { }

  onSubmit(){
    this.authService.signUp(this.email,this.password).then(
      res => {
        console.log('Succesful login;');
          this.router.navigate(['/home'])
        }
      ).catch(
          err => {
            console.log(err);
            this.isError = true;
            this.errorMessage = err.message;
          }
      )  
  }

  ngOnInit(): void {
  }
}