import { DataService } from './../data.service';
import { Component, OnInit } from '@angular/core';
import { Todo } from '../interfaces/todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  fileObj: File;
  fileUrl: string;
  errorMsg: boolean;
  title = 's3-file-uploader-app';



  constructor(private DataService:DataService) { 

  this.errorMsg = false;
}
ngOnInit(): void {
throw new Error('Method not implemented.');
}



onFilePicked(event: Event): void {

  this.errorMsg = false;
  console.log(event);
  const FILE = (event.target as HTMLInputElement).files[0];
  this.fileObj = FILE;
  console.log(this.fileObj);
}

onFileUpload() {

  if (!this.fileObj) {
    this.errorMsg = true;
    return
  }
  
  // const fileForm = new FormData();
  // fileForm.append('file', this.fileObj);
  this.DataService.fileUpload(this.fileObj)
  .toPromise()
  .then(res => {
     console.log("AFTER POST!!")
     
     this.DataService.analyze(this.fileObj.name).
     toPromise()
     .then(res => {
       console.log("ANALYZE RES: ")
       console.log(res)
       if(res['is_registered']=='True'){
         alert('gate is open')
       }
       else{
         alert('not registered car')
       }
       
     })
     .catch(res => {
       console.log("ERR ANALYZE RES: ")
       console.log(res)
     })

   }).catch(err => {
    console.log("ERR AFTER POST!!")
   })
  
}

}


