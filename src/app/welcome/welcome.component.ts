import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  userId;

  navigateToForm() {
    this.router.navigate(['/form']);
  }

  constructor(private router:Router,
              public authService:AuthService) { }


  ngOnInit(): void {
    this.authService.user.subscribe(
      user => {
      this.userId = user.uid;
      
  }
  )
}
}