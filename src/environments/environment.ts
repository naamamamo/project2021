// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBBr-T73Jtd89eRhKhGO2d7xJDTJ6-DY0k",
    authDomain: "project2021-9111b.firebaseapp.com",
    projectId: "project2021-9111b",
    storageBucket: "project2021-9111b.appspot.com",
    messagingSenderId: "666565886323",
    appId: "1:666565886323:web:2f757af8e89b7bd6144506"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
